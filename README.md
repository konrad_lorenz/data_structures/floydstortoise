# Taller: Algoritmo de Floyd's Tortoise and Hare

## Objetivo:
Comprender y aplicar el algoritmo de Floyd's Tortoise and Hare para detectar ciclos en una lista enlazada, buscar números repetidos en una lista con complejidad menor a O(n^2) y agregar la funcionalidad a un microservicio existente generando un nuevo endpoint que reciba un array en el cuerpo de una solicitud JSON.

## Parte 1: Introducción teórica

### 1.1 ¿Qué es el algoritmo de Floyd's Tortoise and Hare?

El algoritmo de Floyd's Tortoise and Hare, también conocido como el algoritmo del puntero lento y rápido, es una técnica utilizada para detectar ciclos en una lista enlazada o secuencia de elementos.

### 1.2 Funcionamiento del algoritmo:

- Inicializamos dos punteros, uno rápido (hare) y otro lento (tortoise), al comienzo de la lista.
- En cada paso, el puntero rápido avanza dos pasos, mientras que el puntero lento avanza un paso.
- Si no hay un ciclo, el puntero rápido eventualmente alcanzará el final de la lista.
- Si hay un ciclo, los punteros se encontrarán en algún punto.
- Para detectar el ciclo, una vez que los punteros se encuentren, movemos uno de los punteros de nuevo al inicio de la lista y luego avanzamos ambos a la misma velocidad (un paso a la vez) hasta que se encuentren nuevamente. El punto de encuentro es el inicio del ciclo.

## Parte 2: Ejercicios prácticos

### 2.1 Implementación del algoritmo

Escribe una función en el lenguaje de programación de tu elección que utilice el algoritmo de Floyd's Tortoise and Hare para detectar un ciclo en una lista enlazada. Puedes utilizar una estructura de datos simple para representar la lista enlazada.

### 2.2 Pruebas de la función

Crea una lista enlazada con y sin ciclo y utiliza la función que implementaste para detectar los ciclos en estas listas.

### 2.3 Análisis de complejidad

Discute la complejidad temporal y espacial del algoritmo de Floyd's Tortoise and Hare. ¿Cuál es su eficiencia en comparación con otros métodos para detectar ciclos en listas enlazadas?

## Parte 3: Búsqueda de Números Repetidos en una Lista

### 3.1 Introducción

El algoritmo de Floyd's Tortoise and Hare también puede aplicarse para encontrar números repetidos en una lista de números sin necesidad de comparar cada par de elementos, lo que resultaría en una complejidad cuadrática.

### 3.2 Implementación del algoritmo

Escriba una función en el lenguaje de programación de su elección que utilice el algoritmo de Floyd's Tortoise and Hare para encontrar un número repetido en una lista de números. La lista puede ser representada como un arreglo o una estructura de datos adecuada.

### 3.3 Pruebas de la función

Cree una lista de números con al menos un número repetido y utilice la función que implementó para encontrar el número repetido en la lista.

### 3.4 Análisis de complejidad

Discuta la complejidad temporal de su algoritmo para buscar números repetidos en comparación con un enfoque cuadrático (O(n^2)). ¿Cómo logra el algoritmo de Floyd's Tortoise and Hare una complejidad más baja?

## Parte 4: Integración en un Microservicio

### 4.1 Integración de la Función

Integre la función de detección de números repetidos al microservicio existente. Cree un nuevo endpoint que acepte un array de números en el cuerpo de una solicitud JSON.

### 4.2 Pruebas del Microservicio

Utilice herramientas como Postman o cURL para enviar solicitudes al nuevo endpoint del microservicio. Asegúrese de que la función detecte correctamente números repetidos en el array proporcionado en el cuerpo de la solicitud.

## Parte 5: Informe IEEE

Cree un informe en formato IEEE que incluya todos los aspectos abordados en este taller, desde la teoría del algoritmo hasta su implementación, pruebas y la integración en el microservicio. Asegúrese de proporcionar documentación detallada sobre el código y resultados de las pruebas.

## Parte 6: Repositorio

Cree un repositorio en línea con un nombre similar al repositorio donde se subirá este documento. El repositorio debe contener el código fuente de la implementación del algoritmo del punto 2, 3 y el informe IEEE.

